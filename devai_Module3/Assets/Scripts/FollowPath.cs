﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float speed = 5.0f;
    float accuracy = 2.0f;
    float rotSpeed = 2.0f;
    public GameObject wpManager;
    GameObject[] wps;
    [SerializeField] GameObject currentNode;
    [SerializeField] int currentWaypointIndex = 0;
    Graph graph;
    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength())
        {
            return;
        }

        //the node we are closest to at the moment
        currentNode = graph.getPathPoint(currentWaypointIndex);

        //if we are close enough to the nearest waypoint, move to the next one
        if (Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position,
                             transform.position) < accuracy)
        {
            currentWaypointIndex++;
        }

        //if we are not at the end of the path
        if (currentWaypointIndex < graph.getPathLength())
        {
            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, 
                                                    Quaternion.LookRotation(direction), 
                                                    Time.deltaTime * rotSpeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    public void goToHelipad()
    {
        graph.AStar(currentNode, wps[0]);
        currentWaypointIndex = 0;
    }
    public void goToRuins()
    {
        graph.AStar(currentNode, wps[19]);
        currentWaypointIndex = 0;
    }
    public void goToFactory()
    {
        graph.AStar(currentNode, wps[10]);
        currentWaypointIndex = 0;
    }
    public void goToTwinMountains()
    {
        graph.AStar(currentNode, wps[2]);
        currentWaypointIndex = 0;
    }
    public void goToBarracks()
    {
        graph.AStar(currentNode, wps[5]);
        currentWaypointIndex = 0;
    }
    public void goToCommandCenter()
    {
        graph.AStar(currentNode, wps[18]);
        currentWaypointIndex = 0;
    }
    public void goToOilPump()
    {
        graph.AStar(currentNode, wps[7]);
        currentWaypointIndex = 0;
    }
    public void goToTankers()
    {
        graph.AStar(currentNode, wps[11]);
        currentWaypointIndex = 0;
    }
    public void goToRadar()
    {
        graph.AStar(currentNode, wps[17]);
        currentWaypointIndex = 0;
    }
    public void goToCommandPost()
    {
        graph.AStar(currentNode, wps[16]);
        currentWaypointIndex = 0;
    }
    public void goToMiddle()
    {
        graph.AStar(currentNode, wps[6]);
        currentWaypointIndex = 0;
    }
}