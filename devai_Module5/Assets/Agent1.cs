﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent1 : AIControl
{
    // Start is called before the first frame update
    override public void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        targetDistance = Vector3.Distance(target.transform.position, this.transform.position);

        if(targetDistance < detectionRadius)
        {
            Pursue();
        }
        else
        {
            Wander();
        }
    }
}
