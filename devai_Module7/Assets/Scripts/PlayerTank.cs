﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTank : Unit
{
    public hpBar hpSlider;
    // Start is called before the first frame update
    override public void Start()
    {
        base.Start();

        hpSlider.setMaxHealth(maxHP);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            StartFiring();
        }
        
        if(Input.GetMouseButtonUp(0))
        {
            StopFiring();
        }
    }
    
    public void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.tag == "bullet")
        {
            currentHP -= 10;
            hpSlider.setHealth(currentHP);
        }

        if(currentHP <= 0)
        {
            Destroy(this.gameObject);
            Time.timeScale = 0;
        }
    }
}
