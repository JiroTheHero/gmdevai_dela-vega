﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : Unit
{
    Animator anim;
    public GameObject player;
    
    public GameObject GetPlayer()
    {
        return player;
    }

    // Start is called before the first frame update
    override public void Start()
    {
        base.Start();
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        anim.SetInteger("currentHP", currentHP);
    }
    public void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.tag == "bullet")
        {
            currentHP -= 10;
        }

        if(currentHP <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
