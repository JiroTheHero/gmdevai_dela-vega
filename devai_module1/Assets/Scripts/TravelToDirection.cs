﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelToDirection : MonoBehaviour
{
    Vector3 direction = new Vector3(8,0,-4);
    float moveSpeed = 5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.Translate(direction.normalized * moveSpeed * Time.deltaTime);
    }
}
